package ru.sokolov.loan_calculator.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.loan_calculator.service.CalculateServiceImpl;
import ru.sokolov.loan_calculator.web.form.FormIn;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(MainController.class)
class MainControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CalculateServiceImpl calculateService;

    @Test
    void getForm() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new FormIn()))
                .andExpect(view().name("Index"));
    }

    @Test
    void getResultIsOk() throws Exception {
        mockMvc.perform(post("/result")
                        .param("loanAmount", "100000")
                        .param("loanTermInMonths", "12"))
                .andExpect(status().isOk())
                .andExpect(view().name("Result"));
    }

    @Test
    void getResultIsNotOk() throws Exception {
        mockMvc.perform(post("/result")
                        .param("loanAmount", "")
                        .param("loanTermInMonths", ""))
                .andExpect(status().isOk())
                .andExpect(view().name("Index"));
    }
}