package ru.sokolov.loan_calculator.api.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.loan_calculator.service.CalculateServiceImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ApiController.class)
class ApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    @SpyBean
    CalculateServiceImpl calculateService;

    @Test
    void postResult() throws Exception {
        mockMvc.perform(post("/api/result")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"loanAmount\": 100000,\n" +
                                "  \"loanTermInMonths\": 12,\n" +
                                "  \"interestRatePerYear\": 15,\n" +
                                "  " +
                                "\"date\": \"2021-09-24\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"payments\": [\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 1,\n" +
                        "      \"paymentDate\": \"2021-10-24\",\n" +
                        "      \"principal\": 7884.88,\n" +
                        "      \"interest\": 1000.00,\n" +
                        "      \"balance\": 100000.00,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 2,\n" +
                        "      \"paymentDate\": \"2021-11-24\",\n" +
                        "      \"principal\": 7963.73,\n" +
                        "      \"interest\": 921.15,\n" +
                        "      \"balance\": 92115.12,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 3,\n" +
                        "      \"paymentDate\": \"2021-12-24\",\n" +
                        "      \"principal\": 8043.37,\n" +
                        "      \"interest\": 841.51,\n" +
                        "      \"balance\": 84151.39,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 4,\n" +
                        "      \"paymentDate\": \"2022-01-24\",\n" +
                        "      \"principal\": 8123.80,\n" +
                        "      \"interest\": 761.08,\n" +
                        "      \"balance\": 76108.03,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 5,\n" +
                        "      \"paymentDate\": \"2022-02-24\",\n" +
                        "      \"principal\": 8205.04,\n" +
                        "      \"interest\": 679.84,\n" +
                        "      \"balance\": 67984.23,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 6,\n" +
                        "      \"paymentDate\": \"2022-03-24\",\n" +
                        "      \"principal\": 8287.09,\n" +
                        "      \"interest\": 597.79,\n" +
                        "      \"balance\": 59779.19,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 7,\n" +
                        "      \"paymentDate\": \"2022-04-24\",\n" +
                        "      \"principal\": 8369.96,\n" +
                        "      \"interest\": 514.92,\n" +
                        "      \"balance\": 51492.10,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 8,\n" +
                        "      \"paymentDate\": \"2022-05-24\",\n" +
                        "      \"principal\": 8453.66,\n" +
                        "      \"interest\": 431.22,\n" +
                        "      \"balance\": 43122.14,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 9,\n" +
                        "      \"paymentDate\": \"2022-06-24\",\n" +
                        "      \"principal\": 8538.20,\n" +
                        "      \"interest\": 346.68,\n" +
                        "      \"balance\": 34668.48,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 10,\n" +
                        "      \"paymentDate\": \"2022-07-24\",\n" +
                        "      \"principal\": 8623.58,\n" +
                        "      \"interest\": 261.30,\n" +
                        "      \"balance\": 26130.29,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 11,\n" +
                        "      \"paymentDate\": \"2022-08-24\",\n" +
                        "      \"principal\": 8709.81,\n" +
                        "      \"interest\": 175.07,\n" +
                        "      \"balance\": 17506.71,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"paymentNumber\": 12,\n" +
                        "      \"paymentDate\": \"2022-09-24\",\n" +
                        "      \"principal\": 8796.91,\n" +
                        "      \"interest\": 87.97,\n" +
                        "      \"balance\": 8796.90,\n" +
                        "      \"payment\": 8884.88\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}"));
    }
}