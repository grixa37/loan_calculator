package ru.sokolov.loan_calculator.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.sokolov.loan_calculator.dto.CreditPayment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(SpringExtension.class)
class CalculateServiceImplTest {

    @InjectMocks
    CalculateServiceImpl sbj;

    @Test
    void getPayments() {
        BigDecimal loanAmount = BigDecimal.valueOf(100_000);
        int loanTermInMonths = 12;
        BigDecimal interestRatePerYear = BigDecimal.valueOf(15);
        LocalDate date = LocalDate.of(2021, 9, 24);

        List<CreditPayment> payments = sbj.getPayments(loanAmount, loanTermInMonths, interestRatePerYear, date);

        assertFalse(payments.isEmpty());
        assertEquals(payments.size(), 12);
        assertEquals(payments.get(11).getPaymentDate(), date.plusMonths(loanTermInMonths));
    }
}