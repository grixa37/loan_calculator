package ru.sokolov.loan_calculator.service;

import org.springframework.stereotype.Service;
import ru.sokolov.loan_calculator.dto.CreditPayment;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static ru.sokolov.loan_calculator.constant.Constants.CALC_SCALE2;
import static ru.sokolov.loan_calculator.constant.Constants.ROUND;

@Service
public class CalculateServiceImpl implements CalculateService {

    public List<CreditPayment> getPayments(final BigDecimal loanAmount,
                                           final int loanTermInMonths,
                                           final BigDecimal interestRatePerYear,
                                           LocalDate date) {

        List<CreditPayment> payments = new ArrayList<>();

        // Формула расчета платежа по аннуитетной схеме x = S * P / (1 - (1 + P) ^ -n )
        // месячная ставка по кредиту  P = L / 100 / 12
        BigDecimal monthlyRate = interestRatePerYear
                .divide(new BigDecimal(100), CALC_SCALE2, ROUND)
                .divide(new BigDecimal(12), CALC_SCALE2, ROUND);

        //  (1 + P) ^ -n
        BigDecimal pow = monthlyRate.add(BigDecimal.ONE).pow(-loanTermInMonths, MathContext.DECIMAL64);

        // 1 - (1 + P) ^ -n
        BigDecimal denominator = BigDecimal.ONE.subtract(pow);

        //Расчет суммы платежа x
        BigDecimal amount = loanAmount.multiply(monthlyRate).divide(denominator, CALC_SCALE2, ROUND);

        BigDecimal base = loanAmount; //Сумма кредита

        for (int i = 0; i < loanTermInMonths; i++) {

            //Начисленные проценты pn = Sn * P
            BigDecimal interest = base.multiply(monthlyRate);

            //Дата платежа
            date = date.plusMonths(1);

            //Создаем объект платежа по кредиту с суммой платежа.
            CreditPayment payment = new CreditPayment(
                    i + 1,
                    date,
                    amount.setScale(CALC_SCALE2, ROUND)
            );

            //Добавляем и округляем Основной долг s = x - pn
            payment.setPrincipal(amount.subtract(interest).setScale(CALC_SCALE2, ROUND));

            //Добавляем и округляем остаток с начисленными процентами
            payment.setInterest(interest.setScale(CALC_SCALE2, ROUND));

            //Добавляем и округляем остаток
            payment.setBalance(base.setScale(CALC_SCALE2, ROUND));

            //Суммируем остаток с начисленными процентами
            base = base.add(interest);

            //Расчитываем остаток на следующий месяц
            base = base.subtract(amount);

            //Добавляем запись
            payments.add(payment);
        }

        return payments;
    }
}
