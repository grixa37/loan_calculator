package ru.sokolov.loan_calculator.service;

import ru.sokolov.loan_calculator.dto.CreditPayment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface CalculateService {

    List<CreditPayment> getPayments(final BigDecimal loanAmount,
                                    final int loanTermInMonths,
                                    final BigDecimal interestRatePerYear,
                                    LocalDate date);
}
