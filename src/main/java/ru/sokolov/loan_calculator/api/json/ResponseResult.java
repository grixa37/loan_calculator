package ru.sokolov.loan_calculator.api.json;

import lombok.Builder;
import lombok.Data;
import ru.sokolov.loan_calculator.dto.CreditPayment;

import java.util.List;

@Data
@Builder
public class ResponseResult {
    private List<CreditPayment> payments;
}
