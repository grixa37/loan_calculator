package ru.sokolov.loan_calculator.api.json;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class RequestResult {

    @Min(value = 100_000, message = "Минимальная сумма 100 000 рублей")
    @Max(value = 5_000_000, message = "Максимальная сумма 5 000 000")
    @NotNull(message = "Поле не может быть пустым!")
    private BigDecimal loanAmount;

    @Min(value = 12, message = "Минимальный срок кредита 12 месяцев")
    @Max(value = 60, message = "Максимальная срок кредита 60 месяцев")
    @NotNull(message = "Поле не может быть пустым!")
    private Integer loanTermInMonths;

    private BigDecimal interestRatePerYear;

    private LocalDate date;
}
