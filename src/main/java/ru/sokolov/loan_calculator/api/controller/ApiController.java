package ru.sokolov.loan_calculator.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sokolov.loan_calculator.api.json.RequestResult;
import ru.sokolov.loan_calculator.api.json.ResponseResult;
import ru.sokolov.loan_calculator.dto.CreditPayment;
import ru.sokolov.loan_calculator.service.CalculateService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ApiController {

    private final CalculateService calculateService;

    @PostMapping("/result")
    public ResponseEntity<ResponseResult> postResult(@RequestBody @Valid RequestResult requestResult) {
        List<CreditPayment> payments = calculateService.getPayments(
                requestResult.getLoanAmount(),
                requestResult.getLoanTermInMonths(),
                requestResult.getInterestRatePerYear(),
                requestResult.getDate()
        );

        return ResponseEntity.ok(ResponseResult.builder().payments(payments).build());
    }
}
