package ru.sokolov.loan_calculator.constant;

import java.math.RoundingMode;

public class Constants {

    public static final int CALC_SCALE2 = 2;

    public static final RoundingMode ROUND = RoundingMode.HALF_UP;
}
