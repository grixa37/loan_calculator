package ru.sokolov.loan_calculator.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sokolov.loan_calculator.dto.CreditPayment;
import ru.sokolov.loan_calculator.service.CalculateService;
import ru.sokolov.loan_calculator.web.form.FormIn;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class MainController {

    private final CalculateService calculateService;

    @GetMapping("/")
    public String getForm(Model model, FormIn form) {
        model.addAttribute("form", form);

        return "Index";
    }

    @PostMapping("/result")
    public String getResult(@ModelAttribute("form")
                            @Valid FormIn form,
                            BindingResult result,
                            Model model) {
        if (!result.hasErrors()) {
            List<CreditPayment> payments = calculateService.getPayments(
                    form.getLoanAmount(),
                    form.getLoanTermInMonths(),
                    form.getInterestRatePerYear(),
                    form.getDate()
            );
            model.addAttribute("payments", payments);

            return "Result";
        } else {

            return "Index";
        }
    }
}
