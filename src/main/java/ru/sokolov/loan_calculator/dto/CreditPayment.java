package ru.sokolov.loan_calculator.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class CreditPayment {

    private final int paymentNumber;               // Номер платежа
    private final LocalDate paymentDate;       // Дата платежа
    private BigDecimal principal;                  // Платеж по основному долгу
    private BigDecimal interest;                   // Платеж по процентам
    private BigDecimal balance;                    // Остаток основного долга
    private final BigDecimal payment;              // Общая сумма платежа
}
